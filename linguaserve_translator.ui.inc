<?php

/**
 * @file
 * Provides LinguaServe Translator ui controller.
 */

/**
 * LignuaServe translator ui controller.
 */
class TMGMTLinguaServeTranslatorUIController extends TMGMTDefaultTranslatorUIController {

  /**
   * Overrides TMGMTDefaultTranslatorUIController::pluginSettingsForm().
   */
  public function pluginSettingsForm($form, &$form_state, TMGMTTranslator $translator, $busy = FALSE) {

    $form['wsdlUrl'] = array(
      '#type' => 'textfield',
      '#title' => t('WebService WSDL-URL'),
      '#default_value' => $translator->getSetting('wsdlUrl'),
      '#description' => t('Please enter the WSDL-URL you received from LinguaServe.'),
    );

    $form['username'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#default_value' => $translator->getSetting('username'),
      '#description' => t('Please enter your LinguaServe Username.'),
    );
    $form['password'] = array(
      '#type' => 'password',
      '#title' => t('Password'),
      '#default_value' => $translator->getSetting('password'),
      '#description' => t('Please enter your LinguaServe Password. Only type in if changed.'),
    );
    $form['project_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Project ID'),
      '#default_value' => $translator->getSetting('project_id'),
      '#description' => t('Please enter your LinguaServe Project ID.'),
    );
    $form['company_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Company ID'),
      '#default_value' => $translator->getSetting('company_id'),
      '#description' => t('Please enter your LinguaServe Company ID.'),
    );
    $form['enabled_language_pairs'] = array(
      '#type' => 'textarea',
      '#title' => t('Enabled Language Pairs'),
      '#default_value' => $translator->getSetting('enabled_language_pairs'),
      '#description' => t('Set for which language pairs the translation should be possible. One pair per line, separated by ->. E.g. en -> de. Use drupal language code here. You can change with Remote Mapping the language code to the correct needed one for Linguaserve.'),
    );

    $form['debug'] = array(
      '#type' => 'checkbox',
      '#title' => t('Debug Mode'),
      '#default_value' => $translator->getSetting('debug'),
    );
    return parent::pluginSettingsForm($form, $form_state, $translator);
  }

  /**
   * Implements TMGMTTranslatorUIControllerInterface::checkoutSettingsForm().
   */
  public function checkoutSettingsForm($form, &$form_state, TMGMTJob $job) {
    $form['priority'] = array(
      '#type' => 'select',
      '#title' => t('Priority'),
      '#options' => array('Normal' => t('Normal'), 'Alta' => t('High'), 'Urgente' => t('Urgent')),
      '#default_value' => $job->getSetting('priority'),
      '#description' => t('Please select the priority.'),
    );
    $form['complete_by'] = array(
      '#type' => 'date',
      '#title' => t('Complete-By date'),
      '#default_value' => $job->getSetting('complete_by'),
      '#description' => t('(Optional) You can set a date, when this job should be finished.'),
    );
    return parent::checkoutSettingsForm($form, $form_state, $job);
  }

  /**
   * Implements TMGMTTranslatorUIControllerInterface::checkoutInfo().
   */
  public function checkoutInfo(TMGMTJob $job) {
    // If the job is finished, it's not possible to import translations anymore.
//    if ($job->isFinished()) {
//      return parent::checkoutInfo($job);
//    }
    $form = array(
//      '#type' => 'fieldset',
//      '#title' => t('Check for new Translations'),
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Check for new Translations'),
      '#submit' => array('linguaserve_translator_checkout_info_form_submit'),
    );

    if ($job->isActive()) {
      $form['resend'] = array(
        '#type' => 'submit',
        '#value' => t('Resend'),
        '#submit' => array('linguaserve_translator_checkout_info_form_submit_resend'),
      );

      $translation_available = TRUE;
      foreach ($job->getItems() as $item) {
        /** @var $item TMGMTJobItem */
        if (!$item->isNeedsReview()) {
          $translation_available = FALSE;
          break;
        }
      }

      if ($translation_available) {
        $form['accept-all'] = array(
          '#type' => 'submit',
          '#value' => t('Accept all'),
          '#submit' => array('linguaserve_translator_checkout_info_form_submit_accept'),
        );
      }
    }

    return $form;
  }


}
