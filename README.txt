SUMMARY
=======

Linguaserve translator plugin for the Translation Management Tools (TMGMT)
project. Allows to use the process provided by Linguaserve to translate content.


Features:

* Translate one or multiple nodes by a few simple mouse clicks
* Integration with the ITS2.0 Standard
* Use advanced translation jobs management tool to submit and review
  translations

The project of course also supports implicitly all the features which are
provided by TMGMT like a feature-rich review process, being able to translate
different sources and more.

REQUIREMENTS
============

Depends on Translation Management Tool (tmgmt)
  http://drupal.org/project/tmgmt


Contract with Linguaserve is required to use the API from the module.

OPTIONAL (but recommended)
--------------------------

Internation Tag Set 2.0 Integration
  http://drupal.org/project/its

INSTALLATION
============

* Install the required module (Translation Management Tool) as usual
  (http://drupal.org/node/70151).

* Install this module as usual, see http://drupal.org/node/70151
  for further information.

CONTACT
=======

Current maintainer:
* Karl Fritsche (kfritsche) - http://drupal.org/user/619702

This project has been sponsored by:
* Cocomore AG
  http://www.cocomore.com

* MultilingualWeb-LT Working Group
  http://www.w3.org/International/multilingualweb/lt/

* European Commission - CORDIS - Seventh Framework Programme (FP7)
  http://cordis.europa.eu/fp7/dc/index.cfm
