<?php
/**
 * @file
 * Formatter Class File to generate XHTML output from a TMGMTJob.
 */


/**
 * Export to XLIFF format.
 */
class TMGMTFileformatLinguaServeXHTML extends XMLWriter implements TMGMTFileFormatInterface {

  /**
   * Contains a reference to the currently being exported job.
   *
   * @var TMGMTJob
   */
  protected $job;

  /**
   * ITS Metadata of all jobs.
   *
   * @var array (tjiid => metaData)
   */
  protected $metaData = array();

  /**
   * Temporary save all IDs to create unique keys.
   *
   * @var array
   */
  protected $uniqueKeys = array();

  /**
   * Current TMGMTJobItem Id.
   *
   * @var int
   */
  protected $currentTjiid;

  /**
   * Get the ITS Rules of the job item.
   *
   * All rules are at the begin of the XML, so this is called before addItem.
   *
   * @param TMGMTJobItem $item
   *   Job Item for which Rules should be added.
   *
   * @return array
   *   Array of themed rules for the given Job Item.
   */
  public function addITSRules(TMGMTJobItem $item) {
    $rules = array();
    if (module_exists('its')) {
      $meta_data = its_tmgmt_item_get_meta_data($item);
      $this->metaData[$item->tjiid] = $meta_data;
      // Add domain meta data.
      foreach ($meta_data as $type => $value) {
        switch ($type) {
          case 'domain':
            if (!empty($value[0]['its'])) {
              $rules[] = theme('its_meta_data_rule', array(
                'rule_name' => 'domainRule',
                'attributes' => array(
                  'selector' => '/h:html/h:body',
                  'domainPointer' => '/h:html/h:head/h:meta[@name=\'DC.subject\']/@content',
                ),
              ));
              $this->startElement('meta');
              $this->writeAttribute('name', 'DC.subject');
              $this->writeAttribute('content', $value[0]['its']);
              $this->endElement();
            }
            break;

          case 'translate':
            foreach ($value as $val) {
              if (!empty($val) && !empty($val['value']) && !empty($val['selector'])) {
                $rules[] = theme('its_meta_data__translate', array('meta_data' => $val));
              }
            }
            break;
        }
      }

      if (!empty($meta_data['loc_note'][0]['note'])) {
        $type = (!empty($meta_data['loc_note'][0]['type'])) ? $meta_data['loc_note'][0]['type'] : 'description';
        $rules[] = theme('its_meta_data_rule', array(
          'rule_name'  => 'locNoteRule',
          'attributes' => array('locNoteType' => $type, 'selector' => '/h:html/h:body'),
          'content'    => '<its:locNote translate="no">' . $meta_data['loc_note'][0]['note'] . '</its:locNote>',
        ));
      }

    }
    return $rules;
  }

  /**
   * Adds a job item to the xml export.
   *
   * @param TMGMTJobItem $item
   *   The job item entity.
   */
  protected function addItem(TMGMTJobItem $item) {

    $this->writeAttribute('id', $item->tjiid);
    $this->writeAttribute('lang', $this->job->source_language);

    if (!empty($this->metaData[$item->tjiid]['translation_agent'])) {
      $agent = explode(';', $this->metaData[$item->tjiid]['translation_agent'][0]['its']);
      if (!empty($agent[0])) {
        $this->writeAttribute('its-org', trim($agent[0]));
      }
      if (!empty($agent[1])) {
        $this->writeAttribute('its-person', trim($agent[1]));
      }
    }
    if (!empty($this->metaData[$item->tjiid]['revision_agent'])) {
      $agent = explode(';', $this->metaData[$item->tjiid]['revision_agent'][0]['its']);
      if (!empty($agent[0])) {
        $this->writeAttribute('its-rev-org', trim($agent[0]));
      }
      if (!empty($agent[1])) {
        $this->writeAttribute('its-rev-person', trim($agent[1]));
      }
    }

    $text = $item->getData();
    // Sort after field weight.
    // TODO: Move to tmgmt_entity and tmgmt_node.
    if (in_array($item->plugin, array('node', 'entity'))) {
      $entity = entity_load($item->item_type, array($item->item_id));
      $entity = $entity[$item->item_id];
      list(, , $bundle) = entity_extract_ids($item->item_type, $entity);
      $fields = field_info_instances($item->item_type, $bundle);
      $extra_fields = field_info_extra_fields($item->item_type, $bundle, 'form');
      foreach (element_children($text) as $field_name) {
        $weight = 20;
        if (!empty($fields[$field_name])) {
          $weight = $fields[$field_name]['widget']['weight'];
        }
        elseif (!empty($extra_fields[$field_name])) {
          $weight = $extra_fields[$field_name]['weight'];
        }
        elseif ($field_name == 'node_title') {
          $weight = $extra_fields['title']['weight'];
        }
        $text[$field_name]['#weight'] = $weight;
      }
    }


    $this->currentTjiid = $item->tjiid;
    $this->addItemContent($text);
  }

  /**
   * Add the Content of a Job Item recursively.
   *
   * @param array $data
   *   Job Item data array
   * @param array $keys
   *   Labels (only internal needed)
   * @param string $field_name
   *   Current field name (only internal)
   */
  protected function addItemContent($data, $keys = array(), $field_name = NULL) {
    if (isset($data['#text'])) {
      $id = $this->getID($keys);
      $this->startElement('div');
      $this->writeAttribute('id', $id);

      if (isset($data['#translate']) && $data['#translate'] == FALSE) {
        $this->writeAttribute('translate', 'no');
      }
      // Do not use full html if there are no rules.
      if (isset($field_name) && isset($this->metaData[$this->currentTjiid]['allowed_characters'][$field_name])) {
        $this->writeAttribute('its-allowed-characters', $this->metaData[$this->currentTjiid]['allowed_characters'][$field_name]);
      }
      else {
        $this->writeAttribute('its-allowed-characters', '.');
      }
      if (isset($field_name) && isset($this->metaData[$this->currentTjiid]['storage_size'][$field_name])) {
        $this->writeAttribute('its-storage-size', $this->metaData[$this->currentTjiid]['storage_size'][$field_name]);
      }

      $text = $data['#text'];

      $options = array(
        'indent' => FALSE,
        'output-xhtml' => TRUE,
        'clean' => TRUE,
        'quote-nbsp' => FALSE,
        'show-body-only' => TRUE,
        'show-warnings' => FALSE,
        'markup' => FALSE,
      );

      $tidy = tidy_parse_string($text, $options, 'utf8');
      $text = substr($tidy->body()->value, 7, -9);

      $this->writeRaw($text);
      $this->endElement();
    }
    else {
      foreach (element_children($data, TRUE) as $key) {
        if (isset($this->metaData[$this->currentTjiid]['allowed_characters'][$key]) || isset($this->metaData[$this->currentTjiid]['storage_size'][$key])) {
          $field_name = $key;
        }
        $child_keys = $keys;
        $child_keys[] = $key;
        $this->addItemContent($data[$key], $child_keys, $field_name);
        $field_name = NULL;
      }
    }
  }

  /**
   * Returns a unique ID for a element.
   *
   * @param array $keys
   *   Array of Strings to build a key.
   *
   * @return string
   *   Unique ID
   */
  protected function getID($keys) {
    if (isset($this->currentTjiid)) {
      $key = $this->currentTjiid . '-' . implode('-', $keys);
    }
    else {
      $key = implode('-', $keys);
    }

    if (isset($this->uniqueKeys[$key])) {
      $unique_key = $key . '-' . $this->uniqueKeys[$key];
      $this->uniqueKeys[$key]++;
    }
    else {
      $this->uniqueKeys[$key] = 1;
      $unique_key = $key;
    }
    return $unique_key;
  }

  /**
   * Implements TMGMTFileExportInterface::export().
   */
  public function export(TMGMTJob $job, $item = NULL) {

    $this->job = $job;
    if (!isset($item)) {
      $items = $job->getItems();
    }
    else {
      $items = array($item);
    }

    $this->openMemory();
    $this->setIndent(TRUE);
    $this->setIndentString('  ');
    $this->startDocument('1.0', 'UTF-8');

    $this->startElement('html');
    $this->writeAttribute('xmlns', 'http://www.w3.org/1999/xhtml');
    $this->writeAttribute('xmlns:its', 'http://www.w3.org/2005/11/its');
    $this->writeAttribute('xmlns:itsx', 'http://www.w3.org/2008/12/its-extensions');
    $this->writeAttribute('its:version', '2.0');

    $this->startElement('head');

    $rules = array();
    foreach ($items as $item) {
      $rules += $this->addITSRules($item);
    }
    if (!empty($rules)) {
      $this->startElement('script');
      $this->writeAttribute('type', 'application/its+xml');
      $this->startElement('its:rules');
      $this->writeRaw("\n      ");

      $attributes = array(
        'ready-at' => format_date(time(), 'custom', 'd/m/Y H:i:s:000 T'),
      );

      $priority = $job->getSetting('priority');
      if (!empty($priority)) {
        switch ($priority) {
          case 'Normal':
            $priority = 1;
            break;

          case 'Alta':
            $priority = 2;
            break;

          case 'Urgente':
            $priority = 3;
            break;

          default:
            $priority = 1;
            break;
        }
      }
      else {
        $priority = 1;
      }
      $attributes['priority'] = $priority . '/3';

      // TODO move to workflow module?
      $complete_by = $job->getSetting('complete_by');
      if (isset($complete_by) && !empty($complete_by['year'])) {
        $attributes['complete-by'] = format_date(gmmktime(15, 00, 00, $complete_by['month'], $complete_by['day'], $complete_by['year']), 'custom', 'd/m/Y H:i:s:000 T');
      }

      // TODO move to workflow module?
      if (module_exists('workflow_translator') && $job->getTranslator() != NULL && $job->getTranslator()->plugin == 'workflow') {
        $attributes['ready-to-process'] = array();
        $active_services = workflow_translator_get_active_services($job->getTranslator());
        foreach ($active_services as $state => $services) {
          if ($state != WORKFLOW_TRANSLATOR_TRANSLATOR || $state != WORKFLOW_TRANSLATOR_AFTER_TRANSLATION) {
            continue;
          }
          foreach ($services as $key => $service) {
            if (isset($service['translator']) && $service['translator']->plugin == 'linguaserve') {
              $attributes['ready-to-process'][] = 'hTranslate, reviseQA';
            }
            else {
              $attributes['ready-to-process'][] = $key;
            }
          }
        }
        if (!$job->getTranslator()->getSetting('auto_accept')) {
          $attributes['ready-to-process'][] = 'hReview';
        }
        $attributes['ready-to-process'] = implode(', ', $attributes['ready-to-process']) . ', publish';
      }
      else {
        $attributes['ready-to-process'] = 'hTranslate, reviseQA';
        if ($job->getTranslator() && !$job->getTranslator()->getSetting('auto_accept')) {
          $attributes['ready-to-process'] .= ', hReview';
        }
        $attributes['ready-to-process'] .= ', publish';
      }

      $rules[] = theme('its_meta_data_rule', array(
        'rule_name'  => 'readinessRule',
        'attributes' => $attributes,
        'namespace'  => 'itsx',
      ));

      foreach ($rules as $rule) {
        $this->writeRaw("  " . $rule . "\n      ");
      }
      // Tag its:rules.
      $this->endElement();
      // Tag script.
      $this->endElement();
    }

    $this->endElement();
    $this->startElement('body');

    foreach ($items as $item) {
      $this->addItem($item);
    }

    $this->endElement();

    $this->endElement();
    $this->endDocument();

    $content = $this->outputMemory(TRUE);
    return $this->postFilter($content);
  }

  /**
   * Normalize a selector (replace //div[@id='asset-ID'] to //body).
   *
   * @param SimpleXMLElement $selector
   *   Selected SimpleXMLElement with the selector.
   *
   * @return string
   *   Normalized selector.
   */
  protected function normalizeSelector($selector) {
    return preg_replace('/\/\/job\[@id=\'\d+\'\]/', '', (string) $selector);
  }

  /**
   * Implements TMGMTFileExportInterface::import().
   */
  public function import($imported_file) {
    // It is not possible to load the file directly with simplexml as it gets
    // url encoded due to the temporary://. This is a PHP bug, see
    // https://bugs.php.net/bug.php?id=61469
    $xml_string = file_get_contents($imported_file);
    $xml = @simplexml_load_string($xml_string);
    $items = array();

    $rules = array();
    foreach ($xml->getDocNamespaces() as $ns => $namespace) {
      if (empty($ns)) {
        $ns = 'h';
      }
      $xml->registerXPathNamespace($ns, $namespace);
    }
    foreach ($xml->xpath("//h:script") as $script) {
      // TODO: Is there a way to get inner content, without <script>?
      $rules_xml = $script->asXML();
      $first = strpos($rules_xml, '>') + 1;
      $rules_xml = substr($rules_xml, $first, strpos($rules_xml, '</script>') - $first);
      $rules_xml = @simplexml_load_string($rules_xml);
      foreach ($rules_xml as $rule) {
        if (drupal_strtolower($rule->getName()) == 'withintextrule') {
          continue;
        }
        $attributes = $rule->attributes();
        switch (drupal_strtolower($rule->getName())) {
          case 'translaterule':
            if (!empty($attributes['selector']) && !empty($attributes['translate'])) {
              $rules['translate'][] = array(
                'selector' => (string) $attributes['selector'],
                'value'    => (string) $attributes['translate'],
              );
            }
            break;

          case 'domainrule':
            if (!empty($attributes['domainPointer'])) {
              $val = $xml->xpath($attributes['domainPointer']);
              if (isset($val[0])) {
                $rules['domain'] = (string) $val[0];
              }
            }
            break;

          case 'locnoterule':
            if (!empty($rule->locNote)) {
              $rules['loc_note'][] = array(
                'note' => (string) $rule->locNote,
                'type' => (string) $attributes['locNoteType'],
              );
            }
            break;

          case 'transprovrule':
            if (isset($attributes['transorg'])) {
              $rules['translation_agent'] = (string) $attributes['transorg'];
            }
            if (isset($attributes['transperson'])) {
              if (empty($rules['translation_agent'])) {
                $rules['translation_agent'] = "";
              }
              $rules['translation_agent'] .= ";" . (string) $attributes['transperson'];
            }
            if (isset($attributes['transrevorg'])) {
              $rules['revision_agent'] = (string) $attributes['transrevorg'];
            }
            if (isset($attributes['transrevperson'])) {
              if (empty($rules['revision_agent'])) {
                $rules['revision_agent'] = "";
              }
              $rules['revision_agent'] .= ";" . (string) $attributes['transrevperson'];
            }
            break;
        }
      }
    }

    foreach ($xml->body as $job) {
      $attributes = $job->attributes();
      $job_item_id = (int) $attributes['id'];
      $this->currentTjiid = $job_item_id;

      if (isset($attributes['its-org'])) {
        $rules['translation_agent'] = (string) $attributes['its-org'];
      }
      if (isset($attributes['its-person'])) {
        if (empty($rules['translation_agent'])) {
          $rules['translation_agent'] = "";
        }
        $rules['translation_agent'] .= ";" . (string) $attributes['its-person'];
      }
      if (isset($attributes['its-rev-org'])) {
        $rules['revision_agent'] = (string) $attributes['its-rev-org'];
      }
      if (isset($attributes['its-rev-person'])) {
        if (empty($rules['revision_agent'])) {
          $rules['revision_agent'] = "";
        }
        $rules['revision_agent'] .= ";" . (string) $attributes['its-rev-person'];
      }

      foreach ($job->div as $item) {
        $attributes = $item->attributes();
        $keys = explode('-', (string) $attributes['id']);
        $item = $this->extractItemsFromXML($item);
        drupal_array_set_nested_value($items, $keys, $item);
      }

      if (module_exists('its')) {
        if (!empty($rules)) {
          $items[$job_item_id]['#meta_data'] = $rules;
        }
      }

    }
    return $items;
  }

  /**
   * Extract the content from the body.
   *
   * @param SimpleXMLElement $items
   *   SimpleXMLElement which represents the data from the XML.
   *
   * @return array
   *   Job Item Data Array.
   */
  protected function extractItemsFromXML($items) {
    $return = array();
    $attributes = $items->attributes('its', TRUE);
    if (!empty($attributes['translate'])) {
      if ($attributes['translate'] != 'yes') {
        $return['#translate'] = FALSE;
      }
    }

    $text = "";
    if ($items->count() > 0) {
      foreach ($items->children() as $child) {
        $text .= $child->asXML();
      }
    }
    else {
      $text = trim((string) $items);
    }

    $return['#text'] = $text;

    return $return;
  }

  /**
   * Post filter processor for exported content.
   *
   * TODO: Many transitional code here, delete when fixed.
   */
  protected function postFilter($content) {
    // MLWLT-110: Dirty fix. We should fix it in editor or prior parsing.
    // Using str_replace over regexp due to performance.
    $content = str_replace('&nbsp;', ' ', $content);
    return $content;
  }

  /**
   * Implements TMGMTFileExportInterface::validateImport().
   */
  public function validateImport($imported_file) {
    // It is not possible to load the file directly with simplexml as it gets
    // url encoded due to the temporary://. This is a PHP bug, see
    // https://bugs.php.net/bug.php?id=61469
    $xml_string = file_get_contents($imported_file);
    $html = new DOMDocument();
    @$html->loadHTML($xml_string);

    if (!$html) {
      return FALSE;
    }

    foreach ($html->getElementsByTagName('body') as $job_item) {
      $job_item_id = $job_item->getAttribute('id');
      $job_item = tmgmt_job_item_load($job_item_id);
      if (!empty($job_item)) {
        $job = $job_item->getJob();
        break;
      }
    }

    if (empty($job)) {
      return FALSE;
    }
    return $job;
  }

}
