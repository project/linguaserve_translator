<?php

class LinguaServeSOAPClient {

  /**
   * WSDL URL of Translation service.
   * @var string
   */
  protected $wsdlUrl = '';

  /**
   * @var SoapClient
   */
  private $client;

  /**
   * File Formatter for the XML Export/Import.
   * @var TMGMTFileFormatInterface
   */
  protected $formatter;

  /**
   * Are we debugging? If true no data will send to linugaserve
   * @var bool
   */
  public $debug = FALSE;

  /**
   * @param string $wsdlURL
   * @param bool $debug
   */
  public function LinguaServeSOAPClient($wsdlURL, $debug = FALSE) {
    module_load_include('inc', 'linguaserve_translator', 'linguaserve_translator.format.XHTML');
    $this->formatter = new TMGMTFileformatLinguaServeXHTML();
    $this->debug = $debug;
    $this->wsdlUrl = $wsdlURL;
    if (!$this->debug) {
      try {
        // with xdebug enabled, wsdl error couldn't be catched
        if (function_exists('xdebug_disable')) xdebug_disable();
        $this->client = new SoapClient($this->wsdlUrl); //, array('exceptions' => true, 'trace' => 1);
        if (function_exists('xdebug_enable')) xdebug_enable();
      }
      catch (Exception $e) {
        watchdog('TMGMT', $e->getMessage(), array(), WATCHDOG_ERROR);
      }
    }
  }

  private function checkClient() {
    if ($this->client == NULL && !$this->debug) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Call this function if you want to use https.
   * On initialization always a non https client will be started.
   */
  public function useHttps() {
    if ($this->debug) return;
    try {
      // with xdebug enabled, wsdl error couldn't be catched
      if (function_exists('xdebug_disable')) xdebug_disable();
      $this->wsdlUrl = str_replace('http://', 'https://', $this->wsdlUrl);
      $this->client = new SoapClient($this->wsdlUrl); //, array('exceptions' => true, 'trace' => 1);
      if (function_exists('xdebug_enable')) xdebug_enable();
    }
    catch (Exception $e) {
      watchdog('TMGMT', $e->getMessage(), array(), WATCHDOG_ERROR);
    }
  }

  private function getCredentials($translator) {
    $parameters = array();
    $parameters['empresaId'] = $translator->getSetting('company_id');
    $parameters['proyectoId'] = $translator->getSetting('project_id');
    $parameters['nombreUsuario'] = $translator->getSetting('username');
    $parameters['contrasenia'] = $translator->getSetting('password');
    return $parameters;
  }

  protected function isLocaleJob($job) {
    $items = $job->getItems();
    if ($items && count($items) > 0) {
      $first_item = current($items);
      if ($first_item->plugin == "locale_source")
        return TRUE;
    }
    return FALSE;
  }

  /**
   * @param $xml String
   * @param $name String
   * @param $job TMGMTJob
   * @return stdClass
   */
  private function saveFileDebug($xml, $name, $job) {
    $directory = file_build_uri('linguaserve/debug');
    $uri = $directory . "/" . $name;
    file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
    file_save_data($xml, $uri, FILE_EXISTS_REPLACE);
    $this->addMessage("(TESTMODE) You can download the !file.", array('!file' => l(t('file'), file_create_url($uri))), 'status', $job);
    $response = new stdClass();
    $response->out = "000 - LOCALE DEBUG!";
    return $response;
  }

  /**
   * Try to send a xml file to Linguaserve.
   *
   * @param string $xml
   *   The XML file to send as string.
   * @param TMGMTJob $job
   *   The corresponding TMGMT Job for error messages.
   * @param array $parameter
   *   Parameters to send Linguaserve.
   *   Needed at least
   *   nombreArchivo - file name
   *   sourceSite
   *   credentials, see getCredentials()
   *
   * @return bool
   *   returns true on success
   *
   * @see getCredentials()
   */
  protected function prepareRequest($xml, $job, $parameter = array()) {
    $response = NULL;

    $directory = file_build_uri('linguaserve');
    $uri = $directory . "/" . $parameter['nombreArchivo'];
    file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
    file_save_data($xml, $uri, FILE_EXISTS_REPLACE);
    $this->addMessage("!file created and ready to send.", array('!file' => l(t('XML file'), file_create_url($uri))), 'status', $job);

    // If file > 250.000 Byte, split in multipart.
    $file_size = drupal_strlen($xml);
    if ($file_size > 250000 && !$this->debug) {
      $total_parts = $file_size / 250000;
      if ($file_size % 250000 != 0) {
        $total_parts++;
      }
      $multipart = "@" . $total_parts . "@" . $file_size;
      $retries = 0;
      for ($part = 0; $part < $total_parts; $part++) {
        $parameter['fileMultipart'] = ($part + 1) . $multipart;
        // The real file encoded as base 64.
        $parameter['archivo'] = base64_encode(drupal_substr($xml, $part * 250000, 250000));
        try {
          $response = $this->client->envioArchivoParaTraducir($parameter);
        }
        catch (Exception $e) {
          $response = new stdClass();
          if ($this->confirmOrderReceived($job, $parameter['numeroPedido'], $parameter['sourceSite'])) {
            $response->out = '000 - Order received, everything is fine';
          }
          else {
            $response->out = '022 - ' . $e->getMessage();
          }
        }

        if (!$this->checkResponse($response, $job) && $retries < 3) {
          if ($retries < 3) {
            $job->addMessage(print_r($response, TRUE), array(), 'warning');
            $retries++;
            $part -= 1;
          }
          else {
            break;
          }
        }
      }
    }
    else {
      // The real file encoded as base64.
      $parameter['archivo'] = base64_encode($xml);
      if ($this->debug) {
        $response = $this->saveFileDebug(base64_decode($parameter['archivo']), $parameter['nombreArchivo'], $job);
      }
      else {
        try {
          $response = $this->client->envioArchivoParaTraducir($parameter);
        }
        catch (Exception $e) {
          $response = new stdClass();
          if ($this->confirmOrderReceived($job, $parameter['numeroPedido'], $parameter['sourceSite'])) {
            $response->out = '000 - Order received, everything is fine';
          }
          else {
            $response->out = '022 - ' . $e->getMessage();
          }
        }
      }
    }

    if (!empty($response) && $this->checkResponse($response, $job)) {
      $this->addMessage("!file send successfully to Linguaserve.", array('!file' => l(t('XML file'), file_create_url($uri))), 'status', $job);
      return TRUE;
    }
    else {
      $this->addMessage("Error while sending !file to Linguaserve.", array('!file' => l(t('XML file'), file_create_url($uri))), 'status', $job);
    }

    return FALSE;
  }

  /**
   * Send File to LinguaServe.
   * @param TMGMTJob $job
   * @return bool true if successful
   */
  public function sendFileForTranslation(TMGMTJob $job) {

    global $base_url, $user;

    if (!$this->checkClient()) return FALSE;

    /** @var TMGMTTranslator $translator */
    $translator = $job->getTranslator();

    $parameter = $this->getCredentials($job);

    $parameter['sourceSite'] = $this->getSourceSite();
    // Alphanumeric code which the client assigns to the sending.
    //$parameter['centroDeCosto'] = "";
    // Department
    //$parameter['departamanto'] = "";
    //$parameter['userLoginGCPropietario'] = $translator->getSetting('username');
    //$parameter['userLoginGCValidador'] = $user->name;
    $parameter['prioridad'] = $job->getSetting('priority');
    $parameter['idiomaPartida'] = $translator->mapToRemoteLanguage($job->source_language);
    $parameter['idiomasDestino'] = $translator->mapToRemoteLanguage($job->target_language);
    // Channel of content (taxonomy, breadcrumbs).
    //$parameter['canal'] = "";
    // Type of the content.
    //$parameter['tipoContenido'] = "";
    $parameter['soapVersion'] = "1.0";

    $errors = FALSE;
    $job->settings['linguaserve_translator_order_id'] = array();
    if ($this->isLocaleJob($job)) {
      $xml = $this->formatter->export($job);

      $unique = $job->tjid . '_literals_' . $job->translator;

      $parameter['numeroPedido'] = variable_get('linguaserve_translator_order_id', 1) + 1;
      variable_set('linguaserve_translator_order_id', $parameter['numeroPedido']);
      $job->settings['linguaserve_translator_order_id'][] = $parameter['numeroPedido'];

      if ($this->debug) {
        $parameter['nombreArchivo'] = $parameter['numeroPedido'] . '_' . $unique . '_' . $job->source_language . '_' . $job->target_language . '.xml';
      }
      else {
        $parameter['nombreArchivo'] = $parameter['numeroPedido'] . '_' . $unique . '.xml';
      }
      // Unique part of the filename.
      $parameter['nombreUnicoArchivo'] = $unique;

      if ($this->prepareRequest($xml, $job, $parameter)) {
        $job->settings['linguaserve_last_download_id'] = array($parameter['numeroPedido'] => $parameter['nombreArchivo']);
        $job->save();
        return TRUE;
      }
      else {
        return FALSE;
      }

    }
    else {
      $job->settings['linguaserve_last_download_id'] = array();
      foreach ($job->getItems() as $item) {
        /** @var $item TMGMTJobItem  */
        $item_id = $item->item_id;

        $entity = entity_load($item->item_type, array($item->item_id));
        if (isset($entity[$item->item_id])) {
          $entity = $entity[$item->item_id];
          $uri = entity_uri($item->item_type, $entity);
          $uri['options']['absolute'] = TRUE;
          // Get URL of the entity.
          $parameter['url'] = url($uri['path'], $uri['options']);
        }

        $parameter['numeroPedido'] = variable_get('linguaserve_translator_order_id', 1) + 1;
        variable_set('linguaserve_translator_order_id', $parameter['numeroPedido']);
        $job->settings['linguaserve_translator_order_id'][$item->tjiid] = $parameter['numeroPedido'];

        $unique = $item_id . '_' . $item->item_type;
        if ($this->debug) {
          $parameter['nombreArchivo'] = $parameter['numeroPedido'] . '_' . $unique . '_' . $job->source_language . '_' . $job->target_language . '.xml';
        }
        else {
          $parameter['nombreArchivo'] = $parameter['numeroPedido'] . '_' . $unique . '.xml';
        }
        // Set unique part of the filename.
        $parameter['nombreUnicoArchivo'] = $unique;

	    $xml = $this->formatter->export($job, $item);

        $errors = $this->prepareRequest($xml, $job, $parameter) && $errors;
        if (!$errors) {
          $job->settings['linguaserve_last_download_id'][$parameter['numeroPedido']] = $parameter['nombreArchivo'];
        }
      }
      $job->save();
    }

    return !$errors;
  }

  /**
   * Checks the response. Returns false for any errors an sends a message.
   * @param object $response SOAP response object
   * @param TMGMTJob $job
   * @return bool true for no errors, false for any error
   */
  public function checkResponse($response, $job = NULL) {
    if ($response instanceof SoapFault) {
      $this->addMessage(print_r($response, TRUE), array(), 'debug', $job);
      return FALSE;
    }
    else {
      list($code, $msg) = $this->splitResponse($response);
      switch ($code) {
        case 0: // executed
        case 21: // order received
          return TRUE;
        case 1: // IP restricted
        case 2: // HTTPS required
        case 3: // service denied
        case 4: // no waiting between configuration
          $this->addMessage('Connection error, please contact LinguaServe - @msg', array('@msg' => $response->out), 'error', $job);
          return FALSE;
        case 10: // customerID not correct
        case 11: // projectID not correct
        case 12: // username not correct
        case 13: // password not correct
          $this->addMessage('Wrong credentials. Please check the translator settings and try again.', array(), 'error', $job);
          return FALSE;
        case 16:
          if (isset($job)) {
            $job->rejected('Wrong parameters - @msg', array('@msg' => $response->out), 'error');
          }
          else {
            $this->addMessage('Wrong parameters - @msg', array('@msg' => $response->out), 'error');
          }
          return FALSE;
        case 20:
          if (isset($job)) {
            $job->submitted('Job already sended.');
          }
          else {
            $this->addMessage('Job already sended.', array(), 'error');
          }
          return FALSE;
        case 22:
          $this->addMessage('Unexpected Error - Please try again later. @msg', array('@msg' => $msg), 'error', $job);
          return FALSE;
        case 30: // No translated file available
          $this->addMessage('No new translated files available.', array(), 'status', $job);
          return FALSE;
        default:
          $this->addMessage('Unexpected Error - Please contact Module Maintainer! - @msg', array('@msg' => $response->out), 'error', $job);
          return FALSE;
      }
    }
  }

  public function splitResponse($response) {
    // 000 - message
    $code = (int) drupal_substr($response->out, 0, 3);
    $data = drupal_substr($response->out, 6);
    return array($code, $data);
  }

  /**
   * Add message to job or if no job given log the message.
   * @param string $msg message
   * @param array $variables variables for message
   * @param string $status status string - error, warning, status, debug
   * @param TMGMTJob|null $job
   */
  private function addMessage($msg, $variables = array(), $status = 'status', $job = NULL) {
    if (isset($job)) {
      $job->addMessage($msg, $variables, $status);
    }
    else {
      switch ($status) {
        case 'status':
          $severity = WATCHDOG_INFO;
          break;
        case 'error':
          $severity = WATCHDOG_ERROR;
          break;
        case 'warning':
          $severity = WATCHDOG_WARNING;
          break;
        case 'debug':
          $severity = WATCHDOG_DEBUG;
          break;
        default:
          $severity = WATCHDOG_NOTICE;
          break;
      }
      watchdog('linguaserve_translator', $msg, $variables, $severity);
    }
  }

  /**
   * Check for new translated files.
   * @param array $data
   * @param TMGMTTranslator|TMGMTJob $object
   *     Job or Translator, needed for the credentials.
   *     If job given, messages will be added there, otherwise it will be loged into watchdog.
   * @return array of file arrays, with following ids
   *     job_id, source_site, source_lang, dest_lang, download_id
   */
  public function checkForTranslatedFiles($data, $object) {
    if (!$this->checkClient()) return FALSE;
    if ($object instanceof TMGMTJob) {
      $translator = $object->getTranslator();
      $job = $object;
    }
    else {
      $translator = $object;
      $job = NULL;
    }
    $return = array();
    $parameters = $this->getCredentials($translator);
    $parameters['listaXml'] = 'y'; // we want a xml list back, not a single item
    $parameters['soapVersion'] = '1.0';

    if ($this->debug) {
      $directory = file_build_uri('linguaserve/debug');
      file_prepare_directory($directory, FILE_CREATE_DIRECTORY);

      $files = file_scan_directory($directory, '/.*xml$/');

      foreach ($files as $file) {
        $file_name = explode('_', $file->name);
        if (empty($return[$file_name[0]])) {
          // download id is [projectId]@[yyyymmdd]@[index]@[fileName]@[fileSize]@[returnId]
          $target_lang = array_pop($file_name);
          $target_lang = substr($target_lang, 0, strrpos($target_lang, '.'));
          $source_lang = array_pop($file_name);
          $tmp = array(
            'order_id' => (string) $file_name[0],
            'source_site' => '',
            'source_lang' => (string) $source_lang,
            'dest_lang' => (string) $target_lang,
            'download_id' => '0@'.date('Ymd').'@0@'.$file->filename.'@'.filesize($file->uri).'@0',
            'uri' => $file->uri,
          );
          $return[$tmp['order_id']] = $tmp;
        }
      }
      return $return;
    }

    $response = $this->client->codigoArchivoTraducidoDisponible($parameters);

    if ($this->checkResponse($response, $job)) {
      list($code, $data) = $this->splitResponse($response);
      $data = html_entity_decode($data);
      $xml = new SimpleXMLElement($data);
      $attributes = $xml->attributes();
      // only one file
      if (!empty($attributes) && empty($xml->archivo)) {
        $tmp = array(
          'order_id' => (string) $attributes['numeroPedido'],
          'source_site' => (string) $attributes['sourceSite'],
          'source_lang' => (string) $attributes['idiomaPartida'],
          'dest_lang' => (string) $attributes['idiomaDestino'],
          'download_id' => (string) $xml,
        );
        $return[$tmp['order_id']] = $tmp;
      }
      // multiple files
      else {
        foreach ($xml->archivo as $file) {
          $attributes = $file->attributes();
          $tmp = array(
            'order_id' => (string) $attributes['numeroPedido'],
            'source_site' => (string) $attributes['sourceSite'],
            'source_lang' => (string) $attributes['idiomaPartida'],
            'dest_lang' => (string) $attributes['idiomaDestino'],
            'download_id' => (string) $file,
          );
          $return[$tmp['order_id']] = $tmp;
        }
      }

    }

    return $return;
  }

  /**
   * Download a translated file and imports the data to the job.
   * @param array $data
   *     file data, like returned from checkForTranslatedFiles
   * @param TMGMTJob|TMGMTTranslator $object
   *     Job or Translator, needed for the credentials.
   *     If job given, messages will be added there, otherwise it will be loged into watchdog.
   * @return bool
   */
  public function downloadTranslatedFile($data, $object) {
    if (!$this->checkClient()) return FALSE;
    if ($object instanceof TMGMTJob) {
      $translator = $object->getTranslator();
      $job = $object;
    }
    else {
      $translator = $object;
      $job = NULL;
    }
    $parameters = $this->getCredentials($translator);
    $parameters['nombreArchivo'] = $data['download_id'];
    $parameters['borrarArchivoDisponible'] = 'n'; // should the status set to Downloaded? (y/n)
    $parameters['soapVersion'] = '1.0';

    $downloadSuccess = FALSE;

    // download id is [projectId]@[yyyymmdd]@[index]@[fileName]@[fileSize]@[returnId]
    $values = explode('@', $data['download_id']);
    $file_name = $values[3];
    $file_size = $values[4];
    // file size is to big, we must split
    if ($file_size > 250000 && !$this->debug) {
      $totalParts = $file_size / 250000;
      if ($file_size % 250000 != 0) {
        $totalParts++;
      }
      $multipart = "@" . $totalParts . "@" . $file_size;
      $xml = "";
      for ($part = 0; $part < $totalParts; $part++) {
        // set multipart parameter [part]@[totalPart]@[fileSize]
        $parameters['fileMultipart'] = ($part + 1) . $multipart;
        $response = $this->client->descargaArchivoTraducido($parameters);
        if ($this->checkResponse($response, $job)) {
          list($code, $xml) = $this->splitResponse($response);
          $xml .= base64_decode($xml);
          // all files loaded
          if ($part + 1 == $totalParts) {
            $downloadSuccess = TRUE;
          }
        }
        // if a error happens cancel it complete and try again later
        else {
          break;
        }
      }
    }
    else {
      if ($this->debug) {
        $xml = file_get_contents($data['uri']);
        $downloadSuccess = TRUE;
      }
      else {
        $response = $this->client->descargaArchivoTraducido($parameters);
        if ($this->checkResponse($response, $job)) {
          list($code, $xml) = $this->splitResponse($response);
          $xml = base64_decode($xml);
          $downloadSuccess = TRUE;
        }
      }

    }

    // if download was successfully save it temporarily
    if ($downloadSuccess && !empty($xml)) {
      $directory = file_build_uri('linguaserve');
      $file_uri = $directory ."/". $data['download_id'] .'.xml';
      file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
      file_save_data($xml, $file_uri, FILE_EXISTS_REPLACE);
      $this->addMessage("Downloaded !file from Linguaserve.", array('!file' => l(t('XML file'), file_create_url($file_uri))), 'status', $job);
    }
    // if download failed, add it to the queue again, for a later retry.
    // cancel it after 3 retries.
    else {
      if (empty($data['retries'])) {
        $data['retries'] = 0;
      }
      else {
        $data['retries']++;
      }
      if ($data['retries'] <= 3) {
        /** @var SystemQueue $queue  */
        $queue = DrupalQueue::get('linguaserve_translator_retrieve_data');
        $queue->createItem($data);
      }
      else {
        watchdog('linguaserve_translator', "!job counldn't be downloaded, because of a error: @msg",
          array(
            '@msg' => $response->out,
            '!job' => l(t('Job !id', array('!id' => $data['job_id'])), 'admin/config/regional/tmgmt/jobs/' . $data['job_id']),
          ),
          WATCHDOG_ERROR);
      }
      return FALSE;
    }

    /** @var TMGMTJob $job */
    $job = $this->formatter->validateImport($file_uri, TRUE);
    if (!empty($job)) {
      $items = $this->formatter->import($file_uri, TRUE);

      $job->setState(TMGMT_JOB_STATE_ACTIVE);

      $accept_translation = FALSE;
      foreach ($job->getItems() as $tjiid => $job_item) {
        if (!empty($items[$tjiid])) {
          /** @var TMGMTJobItem $job_item */
          $item = $items[$tjiid];
          $job_item->setState(TMGMT_JOB_ITEM_STATE_ACTIVE);
          $this->setJobDataItemActive($job_item, $job_item->getData());

          if (!empty($item['#meta_data'])) {
            $meta_data = $item['#meta_data'];
            unset($item['#meta_data']);
            $item = its_tmgmt_set_source_data($item, $meta_data, $job_item);
          }
          $job_item->addTranslatedData($item);
          //TODO: already done by TMGMT
//          if ($job_item->getTranslator()->getSetting('auto_accept')) {
//            $job_item->acceptTranslation();
//          }
          $accept_translation = TRUE;
        }
      }

      if ($accept_translation) {
        $this->acceptTranslation(NULL, $data['download_id'], $object);
        return TRUE;
      }
      else {
        $this->addMessage('Order ID "%orderid" not accepted - ItemIds %itemids are not in corresponding job !job ', array('%orderid' => $data['order_id'], '%itemids' => implode(', ', array_keys($items)), '!job' => l($job->label(), $job->uri())));
        return FALSE;
      }
    }
    else {
      $this->cancelTranslation("Couldn't find corresponding translation job for this translation order", array($data['order_id'] => $file_name), $object, '1');
      $this->addMessage('Order ID "%orderid" canceled - Couldn\'t find corresponding translation job for this translation order', array('%orderid' => $data['order_id']));
      return FALSE;
    }
  }

  /**
   * Set all Status from the Job Data to Pending, so it can be reviewed and translated again
   * @param TMGMTJobItem $job_item
   * @param $data
   * @param array $key
   */
  private function setJobDataItemActive(TMGMTJobItem $job_item, $data, array $key = array()) {
    if (isset($data['#text'])) {
      $job_item->updateData($key, array('#status' => TMGMT_DATA_ITEM_STATE_PENDING));
      return;
    }
    foreach (element_children($data) as $item) {
      $this->setJobDataItemActive($job_item, $data[$item], array_merge($key, array($item)));
    }
  }

  /**
   * @param $message
   * @param $id
   * @param $object TMGMTJob|TMGMTTranslator
   * @param $user_id
   * @return bool
   */
  public function cancelTranslation($message, $order_ids, $object, $user_id) {
    if ($object instanceof TMGMTJob) {
      $translator = $object->getTranslator();
      $job = $object;
    }
    else {
      $translator = $object;
      $job = NULL;
    }

    if ($job && empty($order_ids))
      $order_ids = $job->getSetting('linguaserve_translator_order_id');

    if (empty($order_ids))
      return FALSE;

    $error = FALSE;
    foreach ($order_ids as $order_id => $file_name) {
      $cancel_parameters = $this->getCredentials($translator);
      $cancel_parameters['nombreArchivo'] = $file_name;
      $cancel_parameters['msgCancelacion'] = check_plain($message);
      $cancel_parameters['userLoginGCPropietario'] = $user_id;
      $cancel_parameters['soapVersion'] = '1.0';
      $cancel_parameters['numeroPedido'] = $order_id;

      $parameter['sourceSite'] = $this->getSourceSite();

      if ($this->debug) {
        $directory = file_build_uri('linguaserve/debug');
        $file_uri = $directory ."/". $file_name;
        file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
        $realpath = drupal_realpath($file_uri);
        drupal_unlink($realpath);
        $response = new stdClass();
        $response->out = "000 - DEBUG MODE";
      }
      else
        $response = $this->client->cancelacionTrabajo($cancel_parameters);

      if ($this->checkResponse($response)) {
        $this->addMessage('Order ID "%orderid" canceled - %msg', array('%orderid' => $order_id, '%msg' => $message), 'status', $job);
        $error = $error && TRUE;
      }
      else {
        $error = FALSE;
      }
    }
    return !$error;
  }

  /**
   * @param $message
   * @param $id
   * @param $object TMGMTJob|TMGMTTranslator
   * @return bool
   */
  public function acceptTranslation($message, $id, $object) {
    if ($object instanceof TMGMTJob) {
      $translator = $object->getTranslator();
      $job = $object;
    }
    else {
      $translator = $object;
      $job = NULL;
    }
    $confirm_parameters = $this->getCredentials($translator);
    $confirm_parameters['nombreArchivo'] = $id;
    $confirm_parameters['soapVersion'] = '1.0';
    if ($this->debug) {
      $directory = file_build_uri('linguaserve/debug');
      // download id is [projectId]@[yyyymmdd]@[index]@[fileName]@[fileSize]@[returnId]
      $values = explode('@', $id);
      $file_name = $values[3];
      $file_uri = $directory ."/". $file_name;
      file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
      $realpath = drupal_realpath($file_uri);
      drupal_unlink($realpath);
      $response = new stdClass();
      $response->out = "000 - DEBUG MODE";
    }
    else
      $response = $this->client->confirmacionDescargaArchivoTraducido($confirm_parameters);
    if ($this->checkResponse($response, $job)) {
      if (isset($message))
        $this->addMessage($message, array(), 'status', $job);
      return TRUE;
    }
    return FALSE;

  }

  /**
   * Checks if a order has received, if a timeout or something similar happens.
   * @param $translator TMGMTTranslator
   * @param $order_id int
   * @param string $source_site string
   * @return bool
   */
  public function confirmOrderReceived($object, $order_id, $source_site = '') {
    if ($object instanceof TMGMTJob) {
      $translator = $object->getTranslator();
      $job = $object;
    }
    else {
      $translator = $object;
      $job = NULL;
    }

    $parameters = $this->getCredentials($translator);
    $parameters['numeroPedido'] = $order_id;
    $parameters['soapVersion'] = '1.0';
    if (!empty($source_site))
      $parameters['sourceSite'] = $source_site;

    if ($this->debug) return TRUE;

    try {
      $response = $this->client->confirmacionPedidoRecibido($parameters);
    }
    catch (Exception $e) {
      $response = new stdClass();
      $response->out = "022 - ". $e->getMessage();
    }

    return $this->checkResponse($response, $job);
  }

  /**
   * Call a soap function directly.
   * Mainly used for testing.
   * @param $name string
   * @param $parameters array
   * @return mixed
   *
   * @see LinguaServeSOAPClientTest
   */
  public function _callSoap($name, $parameters) {
    if (!$this->debug)
      return $this->client->{$name}($parameters);
    else
      return NULL;
  }

  private function getSourceSite() {
    global $base_url;
    //commented parameters are optional and therefore currently disabled
    if (preg_match('/https?:\/\/(.*?)\.[a-z]{2,4}(:[0-9]*)?$/', $base_url, $matches)) {
      $sourceSite = $matches[1];
    }
    else {
      $sourceSite = strpos($base_url, 'https') === 0 ? str_replace('https://', '', $base_url) : str_replace('http://', '', $base_url);
    }
    return $sourceSite;
  }

}
