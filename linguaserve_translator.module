<?php

/**
 * @file
 * Module file of the linguaserve translation management module.
 */

/**
 * Implements hook_tmgmt_translator_plugin_info().
 */
function linguaserve_translator_tmgmt_translator_plugin_info() {
  return array(
    'linguaserve' => array(
      'label' => t('Linguaserve translator'),
      'description' => t('Linguaserve Translator service.'),
      'plugin controller class' => 'TMGMTLinguaServeTranslatorPluginController',
      'ui controller class' => 'TMGMTLinguaServeTranslatorUIController',
      'map remote languages' => true,
    ),
  );
}

/**
 * Get the Lingaserve SOAP Client.
 * @param TMGMTTranslator $translator
 * @return LinguaServeSOAPClient
 */
function linguaserve_get_soap_client($translator) {
  $clients = &drupal_static(__FUNCTION__);
  if (!isset($clients[$translator->name])) {
    $clients[$translator->name] = new LinguaServeSOAPClient($translator->getSetting('wsdlUrl'), $translator->getSetting('debug'));
  }
  return $clients[$translator->name];
}

/**
 * Implements hook_cron_queue_info().
 */
function linguaserve_translator_cron_queue_info() {
  $queues['linguaserve_translator_retrieve_data'] = array(
    'worker callback' => 'linguaserve_translator_retrieve_data',
    'time' => 120,
  );
  return $queues;
}

/**
 * Implements a drupal queue worker. Download and imports files available from linguaserve.
 * @see linguaserve_translator_cron_queue_info()
 * @param $data
 */
function linguaserve_translator_retrieve_data($data) {
  $translator = linguaserve_get_translator($data['translator']);

  if ($translator instanceof TMGMTTranslator && $translator->isAvailable()) {
    $client = linguaserve_get_soap_client($translator);
    $client->downloadTranslatedFile($data, $translator);
    $added_jobs_to_queue = variable_get('linguaserve_translator_jobs_in_queue', array());
    unset($added_jobs_to_queue[$data['order_id']]);
    variable_set('linguaserve_translator_jobs_in_queue', $added_jobs_to_queue);
    watchdog('linguaserve_translator', 'Finished processing translation file for Order !job: "%uri"', array('%uri' => $data['download_id'], '!job' => $data['order_id']), WATCHDOG_INFO);
  }
}

/**
 * Implements hook_cron().
 */
function linguaserve_translator_cron() {
  // only do this once every 10 minutes
  $last_check = variable_get('linguaserve_translator_last_download_check', 0);
  $now = time();
  if ($last_check + 0 > $now) {
    return;
  }

  // get all translators which a linguaserve translators
  $translators = linguaserve_get_translator();
  foreach ($translators as $translator) {
    $client = linguaserve_get_soap_client($translator);
    /** @var TMGMTTranslator $translator */
    if ($translator->isAvailable()) {
      $new_files = $client->checkForTranslatedFiles(array(), $translator);
    }
    if (!empty($new_files)) {
      // do not add the same file twice to the queue
      $added_jobs_to_queue = variable_get('linguaserve_translator_jobs_in_queue', array());
      /** @var DrupalQueueInterface $queue */
      $queue = DrupalQueue::get('linguaserve_translator_retrieve_data');
      foreach ($new_files as $file) {
        if (!empty($added_jobs_to_queue[$file['order_id']])) continue;
        $added_jobs_to_queue[$file['order_id']] = $file['order_id'];
        $file['translator'] = $translator->name;
        $queue->createItem($file);
      }
      variable_set('linguaserve_translator_jobs_in_queue', $added_jobs_to_queue);
      watchdog('linguaserve_translator', '%count new files for translation added to Queue.', array('%count' => count($new_files)), WATCHDOG_INFO);
    }
    else {
      watchdog('linguaserve_translator', 'No new translated files.', array(), WATCHDOG_INFO);
    }
  }
  variable_set('linguaserve_translator_last_download_check', $now);
}

/**
 * Get a linguaserve translator plugin.
 * If name parameter is omitted, returns an array of translators.
 * @param null|string $name
 * @return TMGMTTranslator|array The translator or a array of translators.
 */
function linguaserve_get_translator($name = NULL) {
  $translators = &drupal_static(__FUNCTION__);
  if (!isset($translators)) {
    $translators = array();
    $query = new EntityFieldQuery();

    $result = $query
      ->entityCondition('entity_type', 'tmgmt_translator')
      ->propertyCondition('plugin', 'linguaserve')
      ->execute();

    if (!empty($result['tmgmt_translator'])) {
      $tmp = entity_load('tmgmt_translator', array_keys($result['tmgmt_translator']));
      foreach ($tmp as $id => $translator) {
        $translators[$translator->name] = $translator;
      }
    }
  }
  elseif (isset($name) && !isset($translators[$name])) {
    $translators[$name] = tmgmt_translator_load($name);
  }

  if (isset($name)) {
    return $translators[$name];
  }
  else {
    return $translators;
  }
}

/**
 * Translator checkout info form submit function.
 * Checks for available translated files and resets the status, if reset is clicked.
 * @param $form
 * @param $form_state
 */
function linguaserve_translator_checkout_info_form_submit($form, &$form_state) {
  /** @var TMGMTJob $job */
  $job = $form_state['tmgmt_job'];

  $client = linguaserve_get_soap_client($job->getTranslator());
  $new_files = $client->checkForTranslatedFiles(array(), $job);

  $job_items = $job->getItems();
  $downloaded = FALSE;
  foreach($new_files as $order_id => $data) {
    $parts = explode('@', $data['download_id']);
    $file_parts = explode('_', substr($parts[3], 0, strrpos($parts[3], '.')));
    foreach ($job_items as $job_item) {
      /** @var TMGMTJobItem $job_item */
      if ($file_parts[1] == $job_item->item_id && $file_parts[2] == $job_item->item_type) {
        $client->downloadTranslatedFile($data, $job);
        //drupal_set_message(t('New translation added.'));
        $downloaded = TRUE;
      }
    }
  }
  if (!$downloaded) {
    //drupal_set_message(t('No new translations for this item found.'));
  }
}

/**
 * Accept every item.
 * @param $form
 * @param $form_state
 */
function linguaserve_translator_checkout_info_form_submit_accept($form, &$form_state) {
  /** @var TMGMTJob $job */
  $job = $form_state['tmgmt_job'];

  /** @var $item TMGMTJobItem */
  foreach ($job->getItems() as $item) {
    $item->acceptTranslation();
  }
}
/**
 * Translator checkout info form submit function for the Resend button.
 * Resend the file.
 * @param $form
 * @param $form_state
 */
function linguaserve_translator_checkout_info_form_submit_resend($form, &$form_state) {
  /** @var TMGMTJob $job */
  $job = $form_state['tmgmt_job'];

  $client = linguaserve_get_soap_client($job->getTranslator());

  // remove old data, resend new data
  foreach ($job->getItems() as $item) {
    /** @var $item TMGMTJobItem  */
    $data = $item->getData();
    foreach ($data as $key => $value) {
      unset($item->data[$key]);
    }
    $item->save();
  }

  $translator = $job->getTranslator();
  if ($translator->plugin == 'workflow') {
    unset($job->settings['currentService']);
    $job->save();
    $job->requestTranslation();
  }
  else {
    if ($client->sendFileforTranslation($job)) {
      // The translation job has been successfully submitted.
      $job->submitted('The translation job has been re-submitted.');
    }
  }

}

function linguaserve_translator_form_alter(&$form, &$form_state, $form_id) {

  if ('tmgmt_translator_form' == $form_id) {
    $form['#submit'] = array_merge(array('linguaserve_translator_plugin_settings_form_submit'), $form['#submit']);
  }

}

function linguaserve_translator_plugin_settings_form_submit($form, &$form_state) {
  if (isset($form_state['tmgmt_translator']) && $form_state['tmgmt_translator']->plugin == 'linguaserve' && empty($form_state['values']['settings']['password'])) {
    $translator = $form_state['tmgmt_translator'];
    $form_state['values']['settings']['password'] = $translator->getSetting('password');
  }
}

function linguaserve_translator_menu() {
  $items['linguaserve/%'] = array(
    'title' => 'ITS Ruleset for a TMGMTJobItem',
    'page callback' => 'linguaserve_translator_format_its_rules_xml',
    'page arguments' => array(1),
    'type' => MENU_CALLBACK,
    'access callback' => TRUE,
  );

  return $items;
}

function linguaserve_translator_format_its_rules_xml($tjiid) {
  $rules = array();

  $tjiid = substr($tjiid, 0, strpos($tjiid, '.xml'));
  $job_item = tmgmt_job_item_load($tjiid);

  if ($job_item) {
    module_load_include('inc', 'linguaserve_translator', 'linguaserve_translator.format');
    $formatter = new TMGMTFileformatLinguaServeXML();
    $formatter->addITSRules($job_item);
    $translate_rules = $formatter->translateRule[$tjiid];

    $theme_name = 'its_meta_data__translate';
    foreach ($translate_rules as $translate_rule) {
      $html = theme($theme_name, array(
        'type'        => 'translate',
        'meta_data'   => $translate_rule,
        'entity'      => '',
        'entity_type' => 'node',
        'view_mode'   => 'full',
      ));
      $rules[] = $html;
    }
  }

  drupal_add_http_header('Content-Type', 'application/xml');
  echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
  $rules[] = theme('its_meta_data_rule', array('rule_name' => 'withinTextRule', 'attributes' => array('withinText' => 'yes', 'selector' => '//b|//em|//i|//strong|//span')));
  echo theme('its_meta_data_rules', array('rules' => $rules));
}
