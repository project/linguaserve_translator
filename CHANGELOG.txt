linguaserve_translator 1.0-dev, 2013-xx-xx (Development)
--------------------------------------------------------

Issue #2032973 by kfritsche, teflo: Fixed not using remote mapping available since TMGMT alpha3.
Issue #2028669 by kfritsche: Fixed Enabled Language Pairs is not required.
by kfritsche: Added script tag around its:rules
by kfritsche: Drupal Coding Standard Formatter Class
by kfritsche: Deleted non-existing file in .info
by kfritsche: Changed dependency to its (formerly mlw_lt)
by kfritsche: Added README and CHANGELOG
by kfritsche: Fixed items were added multiple times to the queue
by kfritsche: Initial commit
