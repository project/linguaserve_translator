<?php

/**
 * @file
 * Provides LinguaServe Translator plugin controller.
 */

/**
 * LinguaServe translator plugin controller.
 */
class TMGMTLinguaServeTranslatorPluginController extends TMGMTDefaultTranslatorPluginController {

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::isAvailable().
   */
  public function isAvailable(TMGMTTranslator $translator) {
    if ($translator->getSetting('username') && $translator->getSetting('password') &&
      $translator->getSetting('project_id') && $translator->getSetting('company_id') &&
      $translator->getSetting('wsdlUrl') && $translator->getSetting('enabled_language_pairs')
    ) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Implements TMGMTTranslatorPluginControllerInterface::requestTranslation().
   */
  public function requestTranslation(TMGMTJob $job) {
    $client = linguaserve_get_soap_client($job->getTranslator());

    if ($client->sendFileforTranslation($job)) {
      $job->submitted('The translation job has been submitted.');
    }

  }

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::getSupportedTargetLanguages().
   */
  public function getSupportedTargetLanguages(TMGMTTranslator $translator, $source_language) {
    $languages = array();
    $pairs = $translator->getSetting('enabled_language_pairs');
    $pairs = explode("\n", $pairs);
    foreach ($pairs as $lang_pair) {
      $lang_pair = explode("->", $lang_pair);
      $source = trim($lang_pair[0]);
      $dest   = trim($lang_pair[1]);
      if (!isset($languages[$source])) {
        $languages[$source] = array();
      }
      $languages[$source][] = $dest;
    }
    return isset($languages[$source_language]) ? drupal_map_assoc($languages[$source_language]) : array();
  }

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::hasCheckoutSettings().
   */
  public function hasCheckoutSettings(TMGMTJob $job) {
    return TRUE;
  }

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::cancelTranslation().
   */
  public function cancelTranslation(TMGMTJob $job) {
    $last_ids = $job->getSetting('linguaserve_last_download_id');
    if (!empty($last_ids)) {
      global $user;
      $client = linguaserve_get_soap_client($job->getTranslator());
      if ($client->cancelTranslation("User ". $user->uid ." canceled request.", $last_ids, $job, $user->uid))
        $job->cancelled();
      else
        $job->addMessage('Error while canceling order.');
    }
    else {
      $job->cancelled();
    }
  }

}
